//
//  DateFormatter.swift
//  Payday
//
//  Created by Михаил on 08.12.2023.
//

import Foundation

extension DateFormatter {
    
    
    
    static let dateAndTime: DateFormatter = {
        
        let formatter = DateFormatter()
//        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "dd MMMM yyyy HH:mm:ss"
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()

    static let date: DateFormatter = {
        
        let formatter = DateFormatter()
//        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "dd MMMM"
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static func timeFrom(date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }
    
    static func sectionsConvertFrom(date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "ru_RU")
        
        let calendar = Calendar.autoupdatingCurrent
        
        if calendar.isDateInToday(date) {
            return "Сегодня"
            
        } else if calendar.isDateInYesterday(date) {
            return "Вчера"
            
        } else {
            
            dateFormatter.dateFormat = "d MMMM"
            return dateFormatter.string(from: date)
        }
    }
    
    static func convertDateFrom(string: String, submitedDateFormat: String = "dd-MM-yyyy'T'HH:mm:ssZ", dateFormat: String = "HH:mm") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = submitedDateFormat
        let date = dateFormatter.date(from: string) ?? Date()
        dateFormatter.dateFormat = dateFormat
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    
    
    static func convertDateFromByTimeZone(string: String, dateFormat: String = "HH:mm") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd-MM-yyyy'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: string) ?? Date()
        dateFormatter.dateFormat = dateFormat
        let dateString = dateFormatter.string(from: date)
        return dateString
    }

    ///Конвертирует из String в Date
    ///
    /// - Parameter dateString: Строка, представляющая дату.
    /// - Parameter format: Формат даты, используемый для парсинга строки.
    ///
    /// ```
    /// convertToDate(from dateString: "02.04.2023", format: "dd.MM.yyyy") -> 2023-04-02 00:00:00 +0000
    /// ```
    ///
    /// - Returns: Объект Date, соответствующий заданной строке и формату.
    static func convertToDate(from dateString: String, format: String, timeZone: TimeZone? = TimeZone(identifier: "UTC") ) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString) ?? Date()
    }
    
    static func convertToDateByTimeZone(from dateString: String, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString) ?? Date()
    }
    
    ///Конвертирует из Date в String формат "dd.MM.yyyy"
    ///
    /// - Parameter date: Date, формат даты по умолчанию в Swift
    ///
    /// ```
    /// convertToString(from date: 2023-04-02 00:00:00 +0000) -> "02.04.2023"
    /// ```
    ///
    /// - Returns: String в формате "dd.MM.yyyy"
    static func convertToString(from date: Date, format: String = "dd.MM.yyyy", timeZone: TimeZone? = TimeZone.current ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "ru_RU")
        return dateFormatter.string(from: date)
    }
    
    static func convertToStringByTimeZone(from date: Date, format: String = "dd.MM.yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "ru_RU")
        return dateFormatter.string(from: date)
    }
        
    static let serverDateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
        
        return formatter
    }()
    
    static var operation: DateFormatter {
        
        let formatter = DateFormatter()
        formatter.timeStyle = DateFormatter.Style.short//Set time style
        formatter.dateStyle = DateFormatter.Style.long //Set date style
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
  
        return formatter
    }

    static var shortDate: DateFormatter {

        let dateFormatter = DateFormatter()

        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateStyle = DateFormatter.Style.long

        dateFormatter.dateFormat =  "dd.MM.yy"
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "ru_RU")

        return dateFormatter
    }
        
    static let minutsAndSecond: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        return formatter
    }()
    
    static let dateWithoutTime: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "dd.MM.yyyy"
        
        return formatter
    }()
    
    static let dateAndTimeISO8601: DateFormatter = {
        //    2023-05-10T16:38:27Z
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        return formatter
    }()
    
    static let dateAndMonth: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "d MMMM"
        
        return formatter
    }()
    
    static let timeAndSecond: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "HH:mm"
        
        return formatter
    }()
    
    static let historyShortDateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM, E"
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let historyFullDateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy"
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let historyDateAndTimeFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .medium
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let detailFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat =  "d MMMM yyyy"
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let productPeriod: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d/MM"
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let product_Period: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
}
