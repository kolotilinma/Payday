//
//  Color.swift
//  Payday
//
//  Created by Михаил on 08.12.2023.
//

import SwiftUI

extension Color {
    
//    public static var tbkBasicTrueBlack:   Color = Color(.tbkBasicTrueBlack)
//    public static var tbkBasicWhite:       Color = Color(.tbkBasicWhite)
//    public static var tbkBgGreenLight:     Color = Color(.tbkBgGreenLight)
//    public static var tbkBgGreen:          Color = Color(.tbkBgGreen)
//    public static var tbkBlackGrey:        Color = Color(.tbkBlackGrey)
//    public static var tbkCGold:            Color = Color(.tbkCGold)
//    public static var tbkGreyInactive:     Color = Color(.tbkGreyInactive)
//    public static var tbkGreyLight:        Color = Color(.tbkGreyLight)
//    public static var tbkGreyMain:         Color = Color(.tbkGreyMain)
//    public static var tbkGrey:             Color = Color(.tbkGrey)
//    public static var tbkPurpleLight:      Color = Color(.tbkPurpleLight)
//    public static var tbkPurple:           Color = Color(.tbkPurple)
//    public static var tbkRedLight:         Color = Color(.tbkRedLight)
    public static var tbkRed:              Color = Color(red: 1, green: 0.46, blue: 0.34)
//    public static var tbkFacebookBlue:     Color = Color(.tbkFacebookBlue)
//    public static var tbkBlueGreyLight:   Color = Color(.tbkBlueGreyLight)
//    public static var tbkBlueGrey:   Color = Color(.tbkBlueGrey)
}
