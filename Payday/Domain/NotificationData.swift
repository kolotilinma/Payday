//
//  NotificationData.swift
//  Payday
//
//  Created by Михаил on 08.12.2023.
//

import Foundation

struct NotificationData: Codable, Equatable {
    
    static func == (lhs: NotificationData, rhs: NotificationData) -> Bool {
        lhs.id == rhs.id && lhs.company == rhs.company && lhs.type == rhs.type && lhs.amount == rhs.amount && lhs.date == rhs.date && lhs.repeatCounter == rhs.repeatCounter && lhs.ignoreWeekends == rhs.ignoreWeekends
    }
    
    
    let id: UUID
    let company: String
    let type: String
    let amount: Amount
    let date: Date
    let repeatCounter: RepeatCounter
    let ignoreWeekends: Bool
}


extension NotificationData {
    
    var firstAfterToday: Date {
        
        return date.firstAfterToday(repeatCounter: repeatCounter)
    }
    
}


private extension Date {
    
    func firstAfterToday(repeatCounter: RepeatCounter) -> Date {
        
        switch repeatCounter {
            
        case .day:
            
            let current = Date().set(hour: self.hour, minute: self.minute)
            if current > Date() {
                
                return current
                
            } else {
                
                return current.add(component: .day, value: 1)
            }
            
        case .weak:
            
            
            
            return self
            
            
        case .month:
            
            let current = Date().set(day: self.day, hour: self.hour, minute: self.minute).beforeWeekday()
            
            if current > Date() {
                
                return current
                
            } else {
                
                return current.add(component: .month, value: 1)
            }
            
        case .year:
            
            let current = Date().set(month: self.month, day: self.day, hour: self.hour, minute: self.minute)
            
            if current > Date() {
                
                return current
                
            } else {
                
                return current.add(component: .year, value: 1)
            }
            
        case .unknown:
            return self
        }
        
    }
    
}


struct Amount: Codable, Equatable {
    
    var value: Decimal = 0
    var currencyCode = "RUB"
    
    
    var  amountString: String {
        
        return value.formatted(.currency(code: currencyCode).locale(Locale(identifier: "ru_RU")))
    }
}

enum RepeatCounter: String, Codable, Unknownable {
    
    case day
    case weak
    case month
    case year
    
    case unknown
}
