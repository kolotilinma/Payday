//
//  CoreModel.swift
//  Payday
//
//  Created by Михаил on 07.12.2023.
//

import Foundation
import Combine

final class CoreModel {
    
    // MARK: - Interface
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    // MARK: - User
    
    //    let auth: CurrentValueSubject<AuthorizationState, Never>
    //    let user: CurrentValueSubject<UserInfoData?, Never> = .init(nil)
    //    let unreadNotifications: CurrentValueSubject<Int?, Never> = .init(nil)
    
    //    var spinner: Cover?
    
    // MARK: - Services
    
    internal let localAgent: LocalAgentProtocol
    internal let settingsAgent: SettingsAgentProtocol
    
    
    // MARK: - Notification
    
    let notificationsData: CurrentValueSubject<[NotificationData], Never>
    
    // MARK: - Private
    
    private var bindings: Set<AnyCancellable>
    private let queue = DispatchQueue(label: "com.mspmobile.MSP-Mobile.model", qos: .userInitiated, attributes: .concurrent)
    
    init(
        localAgent: LocalAgentProtocol,
        settingsAgent: SettingsAgentProtocol
    ) {
        self.bindings = []
        
        // MARK: - Location agent
        self.localAgent = localAgent
        self.settingsAgent = settingsAgent
        if let notifications = localAgent.load(type: [NotificationData].self) {
            
            self.notificationsData = .init(notifications)
            
        } else {
            
            self.notificationsData = .init([])
        }
        self.bind()
        
    }
    
    func bind() {
        
        notificationsData
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] notifications in
                
                saveNotificationsData(notifications: notifications)
                
            }
            .store(in: &bindings)
        
    }
    
    func saveNotificationsData(notifications: [NotificationData]) {
        
        if let sharedContainer = UserDefaults(suiteName: "group.kolotilinma.Payday") {
            
            // Попытка преобразовать в Data
            do {
                let encoder = JSONEncoder()
                let encodedData = try encoder.encode(notifications)
                sharedContainer.setValue(encodedData, forKey: "SharedDataKey")
                // Сохранение в UserDefaults
//                UserDefaults.standard.set(encodedData, forKey: "SharedDataKey")
            } catch {
                print("Ошибка при кодировании объекта NotificationData: \(error)")
            }
            
        }
        
        if let safedNotifications = localAgent.load(type: [NotificationData].self) {
            
            if safedNotifications != notifications {
                
                try? localAgent.store(notifications)
            }
            
        } else {
            
            try? localAgent.store(notifications)
        }
    }
    
}


extension CoreModel {
    
    static var emptyMock: CoreModel {

        let localContext = LocalAgent.Context(cacheFolderName: "cache", encoder: JSONEncoder(), decoder: JSONDecoder(), fileManager: FileManager.default)
        let localAgent = LocalAgent(context: localContext)

        // MARK: - Settings agent

        let settingsAgent = UserDefaultsSettingsAgent()

        let model = CoreModel(localAgent: localAgent, settingsAgent: settingsAgent)
        
        return model
    }
}
