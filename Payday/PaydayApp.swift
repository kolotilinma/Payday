//
//  PaydayApp.swift
//  Payday
//
//  Created by Михаил on 07.12.2023.
//

import SwiftUI

@main
struct PaydayApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var mainViewModel: MainViewModel {
        
        let model = delegate.model
        return MainViewModel(core: model)
    }
    
    var body: some Scene {
        
        WindowGroup {
            
            MainView(viewModel: mainViewModel)
        }
    }
}
