//
//  AppDelegate.swift
//  Payday
//
//  Created by Михаил on 07.12.2023.
//

import SwiftUI

class AppDelegate: NSObject, UIApplicationDelegate {
    
    var model: CoreModel {
        
        let localContext = LocalAgent.Context(cacheFolderName: "cache", encoder: JSONEncoder(), decoder: JSONDecoder(), fileManager: FileManager.default)
        let localAgent = LocalAgent(context: localContext)
        
        // settings agent
        let settingsAgent = UserDefaultsSettingsAgent()
        
        let model = CoreModel(localAgent: localAgent, settingsAgent: settingsAgent)
        
        return model
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        return true
    }
    
    
}
