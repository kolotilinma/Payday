//
//  MainViewModel.swift
//  Payday
//
//  Created by Михаил on 08.12.2023.
//

import SwiftUI
import Combine

final class MainViewModel: ObservableObject {
    
    @Published var homePath = NavigationPath()
    
    @Published var firstPayment: String?
    @Published var availableAfter: String = ""
//    @Published var timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
//    @Published var counter: CGFloat = 0.0
//    @Published var duration: CGFloat = 99990.1
    
//    @Published var sheet: Sheet?
//    @Published var items: [NotificationItemView.ViewModel]
    
//    let notificationsData: CurrentValueSubject<[NotificationData], Never> = .init([])
    
    private let core: CoreModel
    private var bindings = Set<AnyCancellable>()
    private var timer: Timer?
    private var nextDate: Date?
    
    init(core: CoreModel) {
        
        self.core = core
        bind()
    }
    
    func bind() {
        
        core.notificationsData
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] notifications in
                
                setupNextDate(notifications: notifications)
                
            }
            .store(in: &bindings)
    }
    
    func startCountDown() {
        let timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            self?.calculateAvailableAfterTime()
        }
        self.timer = timer
    }
    
    private func calculateAvailableAfterTime() {
        
        guard let nextDate = nextDate else { return }
        guard nextDate > Date() else {
            setupNextDate(notifications: core.notificationsData.value)
            return
        }
        
        let diffComponents = Calendar.current.dateComponents([.month, .day, .hour,.minute,.second], from: Date(), to: nextDate)
        
        var availableAfterStr = ""
        
        if let month = diffComponents.month, month > 0 {
            availableAfterStr += month < 10 ? "0\(month) месяцев " : "\(month) месяцев "
        }
        
        if let day = diffComponents.day, day > 0 {
            availableAfterStr += day < 10 ? "0\(day) дней " : "\(day) дней "
        }

        if let hour = diffComponents.hour, hour > 0 {
            availableAfterStr += hour < 10 ? "0\(hour):" : "\(hour):"
        }

        if let minute = diffComponents.minute, minute > 0 {
            availableAfterStr += minute < 10 ? "0\(minute):" : "\(minute):"
        }

        if let second = diffComponents.second {
            availableAfterStr += second < 10 ? "0\(second)" : "\(second)"
        }
        
        self.availableAfter = availableAfterStr
    }
    
    func setupNextDate(notifications: [NotificationData]) {
        
        let dateList = notifications.map({ $0.firstAfterToday }).sorted(by: { $0 < $1 })
        
        if let first = dateList.first(where: { $0 > Date() }) {
            
            let formatter = DateFormatter.date
            
            firstPayment = formatter.string(from: first)
            nextDate = first
            availableAfter = ""
            startCountDown()
            
        } else {
            
            timer?.invalidate()
            firstPayment = "Платежей больже нет"
            nextDate = nil
            availableAfter = ""
        }
    }
    
    func createNew() {
         
        navigate(route: .init(type: .list(NotificationsListViewModel(core: core))))
    }
    
    func navigate(route:HomeRoute)  {
        
        homePath.append(route)
    }
    
    func pop()  {
        
        homePath.removeLast()
    }
    
}

