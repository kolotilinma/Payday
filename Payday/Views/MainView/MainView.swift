//
//  MainView.swift
//  Payday
//
//  Created by Михаил on 07.12.2023.
//

import SwiftUI

struct MainView: View {
    
    @ObservedObject var viewModel: MainViewModel
//    @State private var path = NavigationPath()
    
    var body: some View {
        
        NavigationStack(path: $viewModel.homePath) {
            
            ZStack {
                
                VStack(spacing: 20) {
                    
                    if let date = viewModel.firstPayment {
                    
                        HStack {
                            
                            Text("Ближайшее:")
                            
                            Text(date)
                        }
                        
                        Text("До выплаты:")
                        
                        Text(viewModel.availableAfter)
                            .font(.title)
                            .padding(.top, 20)
                        
                    }
                }
            }
            .navigationTitle("Главная")
            .toolbar {
               
                Button("Список") {
                    viewModel.createNew()
                }
            }
            .navigationDestination(for: HomeRoute.self) { routes in
                
                switch routes.type {
                    
                case .list(let list):
                    NotificationsListView(viewModel: list)
                    
                }
                
            }
        }
        
    }
}

#Preview {
    MainView(viewModel: .init(core: .emptyMock))
}


struct HomeRoute: Hashable {
    
    func hash(into hasher: inout Hasher) {
        
        hasher.combine(id)
    }
    
    static func == (lhs: HomeRoute, rhs: HomeRoute) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    let type: Kind
    
    enum Kind {
        
        case list(NotificationsListViewModel)
    }
    
//    case list(NotificationsListViewModel)
}
