//
//  AddNewNotification.swift
//  Payday
//
//  Created by Михаил on 07.12.2023.
//

import SwiftUI
import Combine

final class AddNewNotificationViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var company: String = ""
    @Published var type: String = ""
    @Published var amount = Amount()
    @Published var date: Date = Date()
    @Published var ignoreWeekends: Bool = true
    
    private var id: UUID?
    private let core: CoreModel
    
    internal init(core: CoreModel) {
        
        self.core = core
    }
    
    convenience init(core: CoreModel, notification: NotificationData) {
        
        self.init(core: core)
        self.id = notification.id
        self.company = notification.company
        self.type = notification.type
        self.amount = notification.amount
        self.date = notification.date
        self.ignoreWeekends = notification.ignoreWeekends
    }
    
    func saveNotification() {
        
        guard company.isEmpty == false, type.isEmpty == false else { return }
        
        let notification = NotificationData(id: id ?? UUID(), company: company, type: type, amount: amount, date: date.set(second: 0), repeatCounter: .month, ignoreWeekends: ignoreWeekends)
        
        self.action.send(AddNewNotificationViewModelAction.Save(notification: notification))
        self.action.send(AddNewNotificationViewModelAction.Dismiss())
    }
    
}

struct AddNewNotificationView: View {
    
    @ObservedObject var viewModel: AddNewNotificationViewModel
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        
        NavigationView {
            
            ZStack {
                
                ScrollView {
                    
                    VStack(alignment: .leading, spacing: 24) {
                        
                        VStack(alignment: .leading) {
                            
                            Text("Компания:")
                                .font(.subheadline)
                            
                            TextField("Компания", text: $viewModel.company)
                        }
                        .padding(8)
                        .padding(.horizontal, 8)
                        .overlay(
                            RoundedRectangle(cornerRadius: 16)
                                .stroke(Color(red: 0.88, green: 0.92, blue: 0.95), lineWidth: 1)
                        )
                        
                        VStack(alignment: .leading, spacing: 4) {
                            
                            Text("Тип зарплаты:")
                                .font(.subheadline)
                            
                            TextField("Тип", text: $viewModel.type)
                        }
                        .padding(8)
                        .padding(.horizontal, 8)
                        .overlay(
                            RoundedRectangle(cornerRadius: 16)
                                .stroke(Color(red: 0.88, green: 0.92, blue: 0.95), lineWidth: 1)
                        )
                        
                        VStack(alignment: .leading, spacing: 4) {
                            
                            Text("Сумма зарплаты:")
                                .font(.subheadline)
                            
                            TextField("Зарплата", value: $viewModel.amount.value, format: .currency(code: viewModel.amount.currencyCode).locale(Locale(identifier: "ru_RU")))
                        }
                        .padding(8)
                        .padding(.horizontal, 8)
                        .overlay(
                            RoundedRectangle(cornerRadius: 16)
                                .stroke(Color(red: 0.88, green: 0.92, blue: 0.95), lineWidth: 1)
                        )
                        
                        DatePicker("Дата зачисления", selection: $viewModel.date)
                            .font(.subheadline)
                            .padding(.horizontal, 8)
                        
                        Toggle("Не учитывать выходные", isOn: $viewModel.ignoreWeekends)
                            .font(.subheadline)
                            .padding(.horizontal, 8)
                        
                    }
                    .padding()
                    .padding(.top, 20)
                }
                
                VStack {
                
                    Spacer()
                    
                    Button(action: {
                        
                        viewModel.saveNotification()
                        
                    }) {
                        
                        Text("Сохранить")
                            .foregroundColor(.white)
                            .font(.subheadline)
                            .padding(.vertical, 12)
                            .padding(.horizontal, 16)
                            .frame(maxWidth: .infinity, alignment: .center)
                        
                    }
                    .background(Color.tbkRed)
                    .cornerRadius(8)
                    .padding(.horizontal, 16)
                    
                }
            }
            .onReceive(viewModel.action, perform: { action in
                switch action {
                    
                case _ as AddNewNotificationViewModelAction.Dismiss: dismiss()
                    
                default: break
                }
                
            })
        }
    }
}

enum AddNewNotificationViewModelAction {
    
    struct Save: Action {
        
        let notification: NotificationData
    }
    
    struct Dismiss: Action {}
}

#Preview {
    AddNewNotificationView(viewModel: .init(core: .emptyMock))
}
