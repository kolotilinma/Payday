//
//  NotificationItemView.swift
//  Payday
//
//  Created by Михаил on 08.12.2023.
//

import SwiftUI

extension NotificationItemView {
    
    class ViewModel: ObservableObject, Identifiable {
        
        @Published var title: String
        @Published var subTitle: String
        @Published var salary: String
        @Published var date: String
        
        let id: UUID
        
        internal init(id: UUID, title: String, subTitle: String, salary: String, date: String) {
            
            self.id = id
            self.title = title
            self.subTitle = subTitle
            self.salary = salary
            self.date = date
        }
        
        convenience init(notification: NotificationData) {
            
            let formatter = DateFormatter.date
            self.init(id: notification.id, title: notification.company, subTitle: notification.type, salary: notification.amount.amountString, date: formatter.string(from: notification.firstAfterToday))
        }
    }
}

struct NotificationItemView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        HStack {
            
            VStack(alignment: .leading, spacing: 4) {
                
                Text(viewModel.title)
                    .font(.subheadline)
                
                Text(viewModel.subTitle)
                    .font(.caption)
            }
            
            Spacer()
            
            VStack(alignment: .trailing, spacing: 4) {
                
                Text(viewModel.salary)
                    .font(.subheadline)
                
                Text(viewModel.date)
                    .font(.caption)
            }
        }
        .background(Color.white)
        
    }
}

#Preview {
    NotificationItemView(viewModel: .init(id: UUID(), title: "Trube", subTitle: "Аванс", salary: "100 р.", date: "02.02.23"))
}
