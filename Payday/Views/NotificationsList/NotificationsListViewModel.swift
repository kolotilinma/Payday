//
//  NotificationsListViewModel.swift
//  Payday
//
//  Created by Михаил on 08.12.2023.
//

import Foundation
import Combine

class NotificationsListViewModel: ObservableObject, Identifiable {
    
    @Published var sheet: Sheet?
    @Published var items: [NotificationItemView.ViewModel]
    
    let id: UUID = UUID()
    private let core: CoreModel
    private var bindings = Set<AnyCancellable>()
    
    init(core: CoreModel, items: [NotificationItemView.ViewModel]) {
        
        self.core = core
        self.items = items
    }
    
    convenience init(core: CoreModel) {
        
        self.init(core: core, items: [])
        bind()
        
        
        if let encodedData = UserDefaults(suiteName: "group.kolotilinma.Payday")?.data(forKey: "SharedDataKey") {
            do {
                let decoder = JSONDecoder()
                let decodedData = try decoder.decode([NotificationData].self, from: encodedData)
                
                // Теперь у вас есть объект NotificationData для использования в вашем виджете
                print("Прочитанный объект NotificationData: \(decodedData)")
            } catch {
                print("Ошибка при декодировании данных из UserDefaults: \(error)")
            }
        } else {
            print("Данные не найдены в UserDefaults")
        }
    }
    
    func bind() {
        
        core.notificationsData
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] notifications in
                
                let sorted = notifications.sorted(by: { $0.firstAfterToday < $1.firstAfterToday })
                items = sorted.map({ .init(notification: $0) })
                
            }
            .store(in: &bindings)
    }
    
    func createNew() {
         
        let addNewNotification = AddNewNotificationViewModel(core: core)
        sheet = .init(type: .addNewNotification(addNewNotification))
        bind(addNewNotification)
    }
    
    func onTapItem(itemId: NotificationItemView.ViewModel.ID) {
        
        guard let notification = core.notificationsData.value.first(where: { $0.id == itemId }) else { return }
        let addNewNotification = AddNewNotificationViewModel(core: core, notification: notification)
        sheet = .init(type: .addNewNotification(addNewNotification))
        bind(addNewNotification)
    }
    
    func deleteItem(indexSet: IndexSet) {
        
        indexSet.forEach { index in
            
            guard let index = core.notificationsData.value.firstIndex(where: { $0.id == items[index].id }) else { return }
            core.notificationsData.value.remove(at: index)
        }
    }
    
    func bind(_ add: AddNewNotificationViewModel) {
        
        add.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                
                switch action {
                
                case let payload as AddNewNotificationViewModelAction.Save:
                    
                    if let index = core.notificationsData.value.firstIndex(where: { $0.id == payload.notification.id }) {
                        
                        core.notificationsData.value[index] = payload.notification
                        
                    } else {
                        
                        core.notificationsData.value.append(payload.notification)
                    }
                    
                    
                default:
                    break
                }
            }
            .store(in: &bindings)
        
    }
    
    struct Sheet: Identifiable, Equatable {
        
        static func == (lhs: NotificationsListViewModel.Sheet, rhs: NotificationsListViewModel.Sheet) -> Bool {
            lhs.id == rhs.id
        }
        
        let id = UUID()
        let type: Kind
        
        enum Kind {
            
            case addNewNotification(AddNewNotificationViewModel)
        }
    }
}
