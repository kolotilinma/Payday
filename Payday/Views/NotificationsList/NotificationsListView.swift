//
//  NotificationsListView.swift
//  Payday
//
//  Created by Михаил on 08.12.2023.
//

import SwiftUI

struct NotificationsListView: View {
    
    @ObservedObject var viewModel: NotificationsListViewModel
    
    var body: some View {
        
        List {
            
            ForEach(viewModel.items) { item in
                
                NotificationItemView(viewModel: item)
                    .listRowSeparator(.hidden)
                    .listRowBackground(Color.clear)
//                            .listRowInsets(EdgeInsets())
                    .deleteDisabled(false)
                    .onTapGesture {
                        viewModel.onTapItem(itemId: item.id)
                    }
            }
            .onDelete { indexSet in
                
                viewModel.deleteItem(indexSet: indexSet)
                // TODO: delete items
            }
            
        }
        .listStyle(PlainListStyle())
        .sheet(item: $viewModel.sheet) { sheet in
            
            switch sheet.type {
                
            case .addNewNotification(let addNewNotification):
                AddNewNotificationView(viewModel: addNewNotification)
                
            }
        }
        .navigationTitle("Уведомления")
        .toolbar {
           
            Button("Добавить") {
                viewModel.createNew()
            }
        }
    }
}

#Preview {
    NotificationsListView(viewModel: .init(core: .emptyMock, items: [.init(id: UUID(), title: "Trube", subTitle: "Аванс", salary: "100p.", date: "10.02.23")]))
}
