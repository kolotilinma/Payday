//
//  PaydayWidget.swift
//  PaydayWidget
//
//  Created by Михаил on 09.12.2023.
//

import WidgetKit
import SwiftUI
//import Payday

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), remainingTime: TimeInterval())
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), remainingTime: TimeInterval(60000))
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []
        let notifications = getData()
        
        // Ищем ближайшую будущую дату из уведомлений
        if let closestDate = notifications.map({ $0.firstAfterToday }).min() {
            let currentDate = Date()

            // Проверяем, прошла ли уже ближайшая дата
            let timeUntilClosestDate = closestDate.timeIntervalSince(currentDate)

            // Добавляем одну запись с оставшимся временем до ближайшей даты
            let entry = SimpleEntry(date: currentDate, remainingTime: timeUntilClosestDate)
            entries.append(entry)

            // Используем closestDate за пределами блока
            let timeline = Timeline(entries: entries, policy: .after(closestDate.addingTimeInterval(1)))
            completion(timeline)
        } else {
            // Обработка случая, когда notifications пустые
            let timeline = Timeline(entries: entries, policy: .never)
            completion(timeline)
        }
        
        if let widgetBundleIdentifier = Bundle.main.bundleIdentifier {
            WidgetCenter.shared.reloadTimelines(ofKind: "PaydayWidget")
        }
    }
    
    
    private func getData() -> [NotificationData] {
        
        if let encodedData = UserDefaults(suiteName: "group.kolotilinma.Payday")?.data(forKey: "SharedDataKey") {
            do {
                let decoder = JSONDecoder()
                let decodedData = try decoder.decode([NotificationData].self, from: encodedData)
                
                // Теперь у вас есть объект NotificationData для использования в вашем виджете
                print("Прочитанный объект NotificationData: \(decodedData)")
                return decodedData
            } catch {
                print("Ошибка при декодировании данных из UserDefaults: \(error)")
                return []
            }
        } else {
            print("Данные не найдены в UserDefaults")
            return []
        }
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let remainingTime: TimeInterval
}

struct PaydayWidgetEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        VStack {
            Text("До зарплаты:")
                .multilineTextAlignment(.center)

            // Отображаем таймер
            Text(timeString(time: entry.remainingTime))
                .onAppear {
                    // Начальный запуск таймера
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
                        // Обновляем таймер каждую секунду
                        // Можно добавить код обновления данных здесь, если необходимо
                    }
                }
        }
    }

    // Вспомогательный метод для преобразования временного интервала в отформатированную строку
    private func timeString(time: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour, .minute]
        formatter.unitsStyle = .brief
        formatter.calendar?.locale = Locale(identifier: "ru_RU")
        return formatter.string(from: time) ?? ""
    }
}

struct PaydayWidget: Widget {
    let kind: String = "PaydayWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            if #available(iOS 17.0, *) {
                PaydayWidgetEntryView(entry: entry)
                    .containerBackground(.fill.tertiary, for: .widget)
            } else {
                PaydayWidgetEntryView(entry: entry)
                    .padding()
                    .background()
            }
        }
        .configurationDisplayName("До зарплаты")
        .description("Этот виджет показывает сколько осталось до зарплаты.")
    }
}

#Preview(as: .systemSmall) {
    PaydayWidget()
} timeline: {
    SimpleEntry(date: .now, remainingTime: TimeInterval(600))
    SimpleEntry(date: .now, remainingTime: TimeInterval(600000))
}
