//
//  PaydayWidgetBundle.swift
//  PaydayWidget
//
//  Created by Михаил on 09.12.2023.
//

import WidgetKit
import SwiftUI

@main
struct PaydayWidgetBundle: WidgetBundle {
    var body: some Widget {
        PaydayWidget()
    }
}
