//
//  PaydayWigetBundle.swift
//  PaydayWiget
//
//  Created by Михаил on 09.12.2023.
//

import WidgetKit
import SwiftUI

@main
struct PaydayWigetBundle: WidgetBundle {
    var body: some Widget {
        PaydayWiget()
        PaydayWigetLiveActivity()
    }
}
