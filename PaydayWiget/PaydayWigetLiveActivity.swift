//
//  PaydayWigetLiveActivity.swift
//  PaydayWiget
//
//  Created by Михаил on 09.12.2023.
//

import ActivityKit
import WidgetKit
import SwiftUI

struct PaydayWigetAttributes: ActivityAttributes {
    public struct ContentState: Codable, Hashable {
        // Dynamic stateful properties about your activity go here!
        var emoji: String
    }

    // Fixed non-changing properties about your activity go here!
    var name: String
}

struct PaydayWigetLiveActivity: Widget {
    var body: some WidgetConfiguration {
        ActivityConfiguration(for: PaydayWigetAttributes.self) { context in
            // Lock screen/banner UI goes here
            VStack {
                Text("Hello \(context.state.emoji)")
            }
            .activityBackgroundTint(Color.cyan)
            .activitySystemActionForegroundColor(Color.black)

        } dynamicIsland: { context in
            DynamicIsland {
                // Expanded UI goes here.  Compose the expanded UI through
                // various regions, like leading/trailing/center/bottom
                DynamicIslandExpandedRegion(.leading) {
                    Text("Leading")
                }
                DynamicIslandExpandedRegion(.trailing) {
                    Text("Trailing")
                }
                DynamicIslandExpandedRegion(.bottom) {
                    Text("Bottom \(context.state.emoji)")
                    // more content
                }
            } compactLeading: {
                Text("L")
            } compactTrailing: {
                Text("T \(context.state.emoji)")
            } minimal: {
                Text(context.state.emoji)
            }
            .widgetURL(URL(string: "http://www.apple.com"))
            .keylineTint(Color.red)
        }
    }
}

extension PaydayWigetAttributes {
    fileprivate static var preview: PaydayWigetAttributes {
        PaydayWigetAttributes(name: "World")
    }
}

extension PaydayWigetAttributes.ContentState {
    fileprivate static var smiley: PaydayWigetAttributes.ContentState {
        PaydayWigetAttributes.ContentState(emoji: "😀")
     }
     
     fileprivate static var starEyes: PaydayWigetAttributes.ContentState {
         PaydayWigetAttributes.ContentState(emoji: "🤩")
     }
}

#Preview("Notification", as: .content, using: PaydayWigetAttributes.preview) {
   PaydayWigetLiveActivity()
} contentStates: {
    PaydayWigetAttributes.ContentState.smiley
    PaydayWigetAttributes.ContentState.starEyes
}
